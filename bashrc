#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

RED="\[\033[0;31m\]"
GREEN="\[\033[0;32m\]"
YELLOW="\[\033[0;33m\]"
BLUE="\[\033[0;34m\]"
MAGENTA="\[\033[0;35m\]"
CYAN="\[\033[0;36m\]"
GREY="\[\033[0;37m\]"
RESET="\[\033[0m\]"

PS1="$YELLOW\u$RESET @ $BLUE\h \A$RESET $GREEN\W$RESET > "

# classic fancy aliases

alias ls="ls --color=auto --group-directories-first"
alias lls="ls -lAhS"
alias ll="ls -lAh"
alias l="ls -l"
alias grep="grep --color=auto"

# basic git commands

alias gst="git status"
alias gls="git ls-files"
alias grt="git read-tree"

alias ga="git add"
alias gaa="git add -A"
alias gcm="git commit"
alias gft="git fetch"
alias gpl="git pull"
alias gps="git push"
alias gl="git log --decorate"
alias glo="git log --oneline --decorate"
alias graph="git log --oneline --decorate --graph"
alias gdiff="git diff"
alias gbr="git branch"
alias gch="git checkout"
alias gmg="git merge"
alias grem="git remote"
alias gtag="git tag"
alias gshow="git show"

alias grb="git rebase"
alias garch="git archive --format=zip -v -o"

alias sudo="sudo "
alias vi="vim"
alias mkdirtoday="mkdir $(date +%Y_%m_%d_%H%M)"

# Welcome

echo -e "The Storm is here!\n\nWelcome, $(whoami)\n"
