#----------------------------------------------------------------------------
# Automatic lines
#----------------------------------------------------------------------------

# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}'
zstyle :compinstall filename '/home/tormenta/.zshrc'

autoload -Uz compinit
compinit

# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
# End of lines configured by zsh-newuser-install

#setopt  histignoredups appendhistory autocd notify interactivecomments
#unsetopt beep extendedglob nomatch

# PLUGINS
#[ ! -f ~/.zplug/init.zsh ] && git clone https://github.com/zplug/zplug ~/.zplug
#source ~/.zplug/init.zsh
#
#zplug 'junegunn/fzf-bin', from:gh-r, as:command, rename-to:fzf
#zplug 'zsh-users/zsh-completions'
#
#zplug check || zplug install
#zplug load

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

screenfetch

#----------------------------------------------------------------------------
# Modules
#----------------------------------------------------------------------------

source $HOME/.config/zsh/env.zsh
source $HOME/.config/zsh/theme.zsh
source $HOME/.config/zsh/aliases.zsh
source $HOME/.config/zsh/archbox.zsh


