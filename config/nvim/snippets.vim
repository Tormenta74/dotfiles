" Inlined
autocmd FileType go inoremap ,errnil<Tab> if<Space>err<Space>!=<Space>nil<Space>{<Enter><Enter>}<Esc>kA<Tab><Tab>

" From file
autocmd FileType tex,plaintex inoremap ,table<Tab> <Esc>:0r $HOME/Templates/snippets/latex-table<Cr> 3jA

