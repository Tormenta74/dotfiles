" NeoVim init file based on .vimrc example.
" A Go writing configuration.
" Tormenta <diego.sainzdemedrano@gmail.com>

set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

augroup vimrcEx
	au!

	" For all text files set 'textwidth' to 78 characters.
	autocmd FileType text setlocal textwidth=79

	" When editing a file, always jump to the last known cursor position.
	" Don't do it when the position is invalid or when inside an event handler
	" (happens when dropping a file on gvim).
	autocmd BufReadPost *
				\ if line("'\"") >= 1 && line("'\"") <= line("$") |
				\   exe "normal! g`\"" |
				\ endif

augroup END

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
	command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
				\ | wincmd p | diffthis
endif

if has('langmap') && exists('+langnoremap')
	" Prevent that the langmap option applies to characters that result from a
	" mapping.  If unset (default), this may break plugins (but it's backward
	" compatible).
	set langnoremap
endif

""---------------------------------------------------------------------------
""---------------------------------------------------------------------------

set bg=dark

set backup		    " keep a backup file (restore to previous version)
set undofile		" keep an undo file (undo changes after closing)
set directory=/var/tmp//    " Particularly good
set backupdir=/var/tmp//
set undodir=/var/tmp//
set history=50		        " keep 50 lines of command line history
set ruler		            " show the cursor position all the time
set showcmd		            " display incomplete commands
set incsearch		        " do incremental searching

" Comfy leader key
let mapleader=" "

" Some magic (use to facilitate navigation)
set path+=**

" Highlighting
filetype plugin indent on
syntax on
set hlsearch

" Commodities
set number relativenumber
set mouse=a
set autoindent
set splitbelow splitright

" C switch-case indentation option
set cinoptions+=:0

" Autocontinue comments on new lines
set formatoptions+=cro

" Limit that text width
"set colorcolumn=80
"match Error /\%>79v/

" General tab behavior
"set noexpandtab tabstop=4 shiftwidth=4
set expandtab tabstop=4 shiftwidth=4


""---------------------------------------------------------------------------
"" Mappings and abbreviations
""---------------------------------------------------------------------------


" Easy toggle highlight search
nnoremap <Leader>h :set hlsearch!<CR>

" Trailing whitespace
nnoremap <Leader>rt :%s/\s\+$//e<CR>

" Splits navigations
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Map Esc to exit terminal mode:
tnoremap <Esc> <C-\><C-n>

" No one is really happy until you have this shortcuts
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev WQa wqa
cnoreabbrev Wqa wqa
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall
cnoreabbrev Qa qa

""---------------------------------------------------------------------------
"" Statusline
""---------------------------------------------------------------------------

"set statusline=%f
"set statusline+=%m
"
"set statusline+=%=
"set statusline+=%y
"set statusline+=\ ㏑\ %l/%L\ (%p%%)

""---------------------------------------------------------------------------
"" Snippets
""---------------------------------------------------------------------------
"source $HOME/.config/nvim/snippets.vim

""---------------------------------------------------------------------------
" Plugins

" Install Vim Plug if not installed
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall
endif

call plug#begin()

	" Bracket/quotation autoclose
	Plug 'jiangmiao/auto-pairs'
	" Autocolor hex/rgba codes
	Plug 'lilydjwg/colorizer'
	" Nice
	Plug 'morhetz/gruvbox'
	" Sweet bars
	Plug 'vim-airline/vim-airline'

	"" Completion / Syntax
	" LSP support
	"Plug 'neoclide/coc.nvim', {'tag': '*', 'do': './install.sh'}

	"" Language specific
	"" Tex
	Plug 'lervag/vimtex'
	
	"" Go
	" Go syntax
	Plug 'fatih/vim-go'


	""" Do this at some point
	" Finder
	"Plug 'kien/ctrlp.vim'
	"Plug 'junegunn/fzf.vim' " Not yet
	" Dark powered neo-completion
	"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
	" Dark powered neo-code linting
	"Plug 'neomake/neomake'
	"Plug 'zchee/deoplete-go', {'build': {'unix': 'make'}}
	"" TypeScript
	" TypeScript syntax
	"Plug 'leafgarland/typescript-vim'
	"Plug 'HerringtonDarkholme/yats.vim'
	"Plug 'mhartington/nvim-typescript', {'do': './install.sh'}

call plug#end()

""
"" Plugin related configs
""

""---------------------------------------------------------------------------
"" NERDTree
""---------------------------------------------------------------------------
"map <Leader>n :NERDTreeToggle<CR>
"let g:NERDTreeShowHidden=1

""---------------------------------------------------------------------------
"" Autopairs
""---------------------------------------------------------------------------
let g:AutoPairs = {'(':')', '[':']', '{':'}'}

""---------------------------------------------------------------------------
"" Airline
""---------------------------------------------------------------------------
set laststatus=2
let g:powerline_pycmd="py3"
let g:airline_powerline_fonts = 0
let g:airline_powerline_left_sep = ''
let g:airline_powerline_right_sep = ''

""---------------------------------------------------------------------------
"" Neomake
""---------------------------------------------------------------------------
"autocmd! BufWritePost,BufEnter * Neomake

" close preview pane
"autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

""---------------------------------------------------------------------------
"" Vim-Go
""---------------------------------------------------------------------------
let g:go_fmt_command = "goimports"

""---------------------------------------------------------------------------
"" Deoplete
""---------------------------------------------------------------------------
" Use deoplete.
"let g:deoplete#enable_at_startup = 1
"let g:deoplete#sources#go#gocode_binary = '$GOPATH/bin/gocode'
"call deoplete#custom#option('omni_patterns', { 'go': '[^. *\t]\.\w*' })

""---------------------------------------------------------------------------
"" CoC
""---------------------------------------------------------------------------
"" if hidden is not set, TextEdit might fail.
"set hidden
"" Smaller updatetime for CursorHold & CursorHoldI
"set updatetime=300
"" don't give |ins-completion-menu| messages.
"set shortmess+=c
"" always show signcolumns
"set signcolumn=yes
"
"" Use tab for trigger completion with characters ahead and navigate.
"" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
"inoremap <silent><expr> <TAB>
"      \ pumvisible() ? "\<C-n>" :
"      \ <SID>check_back_space() ? "\<TAB>" :
"      \ coc#refresh()
"inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
"
"function! s:check_back_space() abort
"  let col = col('.') - 1
"  return !col || getline('.')[col - 1]  =~# '\s'
"endfunction
"
"" Use <c-space> to trigger completion.
"inoremap <silent><expr> <c-space> coc#refresh()
"
"" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
"" Coc only does snippet and additional edit on confirm.
"inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"
"" Remap keys for gotos
"nmap <silent> gd <Plug>(coc-definition)
"nmap <silent> gy <Plug>(coc-type-definition)
"nmap <silent> gi <Plug>(coc-implementation)
"nmap <silent> gr <Plug>(coc-references)
"
"" Use K to show documentation in preview window
"nnoremap <silent> K :call <SID>show_documentation()<CR>
"function! s:show_documentation()
"  if (index(['vim','help'], &filetype) >= 0)
"    execute 'h '.expand('<cword>')
"  else
"    call CocAction('doHover')
"  endif
"endfunction
"
"" Remap for rename current word
"nmap <leader>rn <Plug>(coc-rename)
"
"" Remap for format selected region
"xmap <leader>f  <Plug>(coc-format-selected)
"nmap <leader>f  <Plug>(coc-format-selected)

""---------------------------------------------------------------------------
"" colorscheme
""---------------------------------------------------------------------------
" Just in case it works
" According to everyone, this is to be done before instantiating the
" colorscheme for some reason
let g:gruvbox_contrast_dark = 'hard'
colorscheme gruvbox
set background=dark    " Setting dark mode

" /Plugins
""---------------------------------------------------------------------------
