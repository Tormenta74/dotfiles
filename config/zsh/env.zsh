export VISUAL=nvim
export EDITOR=$VISUAL

export BROWSER=firefox
export TERMINAL=termite
export TERM=xterm-256color

export JAVA_HOME=/usr/lib/jvm/default
#export JAVA_HOME=/usr/lib/jvm/java-8-openjdk

export GOPATH=$HOME/go

export PATH="$GOPATH/bin:$PATH"
export PATH="$HOME/.scripts:$PATH"
export PATH="$HOME/node/node_modules/.bin:$PATH"

export NVM_DIR=/opt/nvm
source /usr/share/nvm/nvm.sh
source /usr/share/nvm/install-nvm-exec

#export FZF_COMPLETION_TRIGGER="º"
#export FZF_DEFAULT_COMMAND="fd --type f --hidden --follow --exclude .git"
export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'
export FZF_DEFAULT_OPTS="--layout=reverse --height 40%"

