#
# Edit this file accordingly to the system OS, configuration, etc.
#

#----------------------------------------------------------------------------
# Quirks
#----------------------------------------------------------------------------

# I got used to Ctrl+S to save from VSCode, so now I do it all the frickin'
# time
stty -ixon

#----------------------------------------------------------------------------
# termite compatible bindkeys
#----------------------------------------------------------------------------

bindkey -e
bindkey '^[[1;5C' forward-word      # [Ctrl-RightArrow] - move forward one
                                    # word
bindkey '^[[1;5D' backward-word     # [Ctrl-LeftArrow] - move backward one
                                    # word
bindkey '^[OH' beginning-of-line
bindkey '^[[H' beginning-of-line
bindkey '^[OF' end-of-line
bindkey '^[[F' end-of-line
bindkey '^[[3~' delete-char
bindkey '^[[Z' reverse-menu-complete
