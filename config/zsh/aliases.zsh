#----------------------------------------------------------------------------
# Aliases. Lot's of aliases.
#----------------------------------------------------------------------------

# the classics

alias ls="ls --color=auto --group-directories-first"
alias l="ls -l"
alias ll="ls -lAh"
alias lls="ls -lAhS"
alias tree="tree --dirsfirst -aFC -I .git"

alias grep="grep --color=auto"
#alias less="less -N"
alias mkdirtoday="mkdir $(date +%Y_%m_%d)"

# Nice to have
#
alias g="git"
alias sudo="sudo "
alias vi="vim"
alias vim="nvim"
alias vimrc="$EDITOR $HOME/.vimrc"
alias nvimrc="$EDITOR $HOME/.config/nvim/init.vim"
alias zshrc="$EDITOR $HOME/.zshrc"
alias i3conf="$EDITOR $HOME/.config/i3/config"
alias cat="bat -p"
alias truth="source $HOME/.zshrc"
alias tmux="tmux -f $HOME/.config/tmux/tmux.conf"

