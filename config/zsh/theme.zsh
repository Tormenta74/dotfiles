#----------------------------------------------------------------------------
# Theme
#----------------------------------------------------------------------------

source /usr/share/git/git-prompt.sh

export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWSTASHSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
export GIT_PS1_SHOWCOLORHINTS=true
export GIT_PS1_SHOWUPSTREAM="auto git"

#TODO: hasjobs ()
hasjobs () {
	jobs_num = $(jobs -l | wc -l)
	if [[ ! $jobs_num = 0 ]]; then
		echo "I have a job, mom!"
	fi
}
#TODO: repeat_cols () # hint: `tput cols`
#for _ in $(seq `tput cols`)
#do
#echo "Why hello there"
#done 

precmd () {
	__git_ps1 "%F{198%}%n%f %F{red%}職位%F{blue%}【 %F{white%}%~%f%F{blue%} 】%f" "
%F{35%} ➤ %f" "〘 %s 〙"
}

#PROMPT="%F{black%}black%f %F{red%}red%f %F{green%}green%f %F{yellow%}yellow%f %F{blue%}blue%f %F{magenta%}magenta%f %F{cyan%}cyan%f %F{white%}white%f
#%K{black%}black%k %K{red%}red%k %K{green%}green%k %K{yellow%}yellow%k %K{blue%}blue%k %K{magenta%}magenta%k %K{cyan%}cyan%k %K{white%}white%k
#" 
RPROMPT="%F{35%}⮜ %f %?『 %j 』%D{%T}"
