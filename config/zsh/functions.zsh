
isrunning() {
    if [[ $# == 0 ]]; then
        echo -e "No program name given"
    else
		ps="$(ps -eo euser,pid,cputime,args)"
        echo $ps | head -n1
        echo $ps | grep $1 -i | grep -v "grep"
    fi
}

# Thanks, Luke Smith!
sedit() {
	du -a $SCRIPT_PATH | awk '{print $2}' | fzf | xargs $EDITOR
}

