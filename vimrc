" Vimrc
" Tormenta74 <diego.sainzdemedrano@gmail.com>

set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

augroup vimrcEx
    au!

    " For all text files set 'textwidth' to 78 characters.
    autocmd FileType text setlocal textwidth=78

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    autocmd BufReadPost *
                \ if line("'\"") >= 1 && line("'\"") <= line("$") |
                \   exe "normal! g`\"" |
                \ endif

augroup END

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
                \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langnoremap')
    " Prevent that the langmap option applies to characters that result from a
    " mapping.  If unset (default), this may break plugins (but it's backward
    " compatible).
    set langnoremap
endif

""---------------------------------------------------------------------------
""---------------------------------------------------------------------------

" Fallback colorscheme (should load gruvbox after plugin load)
set bg=dark
colorscheme r

set backup		    " keep a backup file (restore to previous version)
set undofile		" keep an undo file (undo changes after closing)
set directory=/var/tmp//    " Particularly good
set backupdir=/var/tmp//
set undodir=/var/tmp//
set history=50		        " keep 50 lines of command line history
set ruler		            " show the cursor position all the time
set showcmd		            " display incomplete commands
set incsearch		        " do incremental searching

" Comfy leader key
let mapleader=","

" Some magic (use to facilitate navigation)
set path+=**

" Highlighting
syntax on
set hlsearch

" Commodities
set relativenumber
set mouse=a
filetype plugin indent on
set autoindent

" C switch-case indentation option
set cinoptions+=:0

" General tab behavior
set expandtab tabstop=4 shiftwidth=4


""---------------------------------------------------------------------------
"" Mappings and abbreviations
""---------------------------------------------------------------------------

" Different indentation settings
nmap <Leader><C-t> :set expandtab tabstop=2 shiftwidth=2<CR>
nmap <Leader>t :set expandtab tabstop=4 shiftwidth=4<CR>
nmap <Leader>T :set noexpandtab tabstop=8 shiftwidth=8<CR>

" Folding
nnoremap <Space> za

" Easy toggle highlight search
nnoremap <Leader>h :set hlsearch!<CR>

" Easy moving through the tags
nnoremap <C-p> gt

" Trailing whitespace
nnoremap <Leader>rt :%s/\s\+$//e<CR>

" Map Esc to exit terminal mode:
tnoremap <Esc> <C-\><C-n>

" No one is really happy until you have this shortcuts
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev WQa wqa
cnoreabbrev Wqa wqa
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall
cnoreabbrev Qa qa

" Automatic statusline
set ls=2
let g:powerline_pycmd="py3"

""---------------------------------------------------------------------------
" Install Vim Plug if not installed
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall
endif

call plug#begin()

Plug 'Valloric/MatchTagAlways', { 'for': ['html', 'xml'] } " Highlight cursor containing tag
Plug 'jiangmiao/auto-pairs' " Bracket/quotation autoclose
Plug 'lilydjwg/colorizer' " Autocolor hex/rgba codes
Plug 'mattn/emmet-vim', { 'for': ['html', 'php', 'xml', 'eruby', 'markdown'] } " Sweet tag completion
Plug 'morhetz/gruvbox' " Nice
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' } " File tree
Plug 'vim-airline/vim-airline' " Sweet bars
Plug 'vim-airline/vim-airline-themes'

""
"" Plugin related configs
""

""---------------------------------------------------------------------------
"" NERDTree
""---------------------------------------------------------------------------
map <Leader>n :NERDTreeToggle<CR>
let g:NERDTreeShowHidden=1

""---------------------------------------------------------------------------
"" Emmet
""---------------------------------------------------------------------------
let g:user_emmet_mode='a'   " Set tag completion for all modes

""---------------------------------------------------------------------------
"" Autopairs
""---------------------------------------------------------------------------
let g:AutoPairs = {'(':')', '[':']', '{':'}', '`':'`'}

""---------------------------------------------------------------------------
"" Airline
""---------------------------------------------------------------------------
set laststatus=2
let g:airline_powerline_fonts = 0
let g:airline_powerline_left_sep = ''
let g:airline_powerline_right_sep = ''

""---------------------------------------------------------------------------
"" gruvbox
""---------------------------------------------------------------------------
" Just in case it works
colorscheme gruvbox

call plug#end()
""---------------------------------------------------------------------------

" Show me dat beautiful background
highlight Normal guibg=NONE ctermbg=NONE
